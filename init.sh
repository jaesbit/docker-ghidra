#Link utils to home
ln -sf $GHIDRA_INSTALL_PATH/ghidra/server/svrAdmin $HOME/svrAdmin 
ln -sf $GHIDRA_INSTALL_PATH/ghidra/server/svrInstall $HOME/svrInstall 
ln -sf $GHIDRA_INSTALL_PATH/ghidra/server/svrUninstall $HOME/svrUninstall 
ln -sf $GHIDRA_INSTALL_PATH/ghidra/server/ghidraSvr $HOME/ghidraSvr 

#Be able to customize settings by shared folder

#copy if not exists server.conf to home path
echo n\n | cp -i $GHIDRA_INSTALL_PATH/ghidra/server/server.conf $HOME/server.conf 

#link server conf into GHIDRA INSTALL PATH
ln -sf $HOME/server.conf $GHIDRA_INSTALL_PATH/ghidra/server/server.conf 

$GHIDRA_INSTALL_PATH/ghidra/server/ghidraSvr console